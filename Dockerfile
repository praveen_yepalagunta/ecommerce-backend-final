FROM maven:latest as build
WORKDIR /app
COPY pom.xml /app
COPY src ./src
RUN mvn clean install package

FROM tomcat:9.0.73-jdk17
COPY --from=build /app/target/*.war /usr/local/tomcat/webapps/
